[![](https://img.shields.io/badge/IBM%20Cloud-powered-blue.svg)](https://cloud.ibm.com) [![](https://img.shields.io/badge/firefox-%3E%3D65-brightgreen)](https://www.mozilla.org/pt-BR/firefox/new/) [![](https://img.shields.io/badge/chrome-%3E%3D20-brightgreen)](https://www.google.com/intl/pt-BR/chrome/)

# Desafio Data Science Banco do Brasil

* [1. Introdução](#1-introdução)
* [2. Avaliação](#2-avaliação)
* [3. Requisitos](#3-requisitos)
* [4. Informações sobre limites do Watson Studio](#4-informações-sobre-limites-do-watson-studio)
* [5. Desafio](#5-desafio)

## 1. Introdução (EM CONSTRUÇÃO)
Video Banco do Brasil

[![Watch the video](https://i.imgur.com/vKb2F1B.png)](https://www.youtube.com/embed/t-w-XSbVDsI)

Bem-vindos ao desafio do Programa Ascensão Profissional em Inteligência Analítica e Artificial. Atualmente, sabemos que a quantidade e a qualidade de dados gerados por uma empresa são diferenciais estratégicos, associados ao sucesso e a perenidade, principalmente em mercados tão competitivos como o bancário.
Nesse cenário, o analytics é indutor de transformação e, consequentemente, da sustentabilidade de uma empresa. Diante disso, o programa visa identificar, desenvolver e reter talentos com conhecimentos específicos em Inteligência Analítica e Artificial dispostos a contribuir com o alcance de nossos resultados. Vamos assumir esse desafio!

## 2. Avaliação

Por se tratar de um desafio de data science o que será avaliado é a qualidade do modelo criado pelo competidor. Ao final dos notebook fornecidos, existe uma célula cuja função é submeter sua solução para os sistemas de avaliação criados. Uma vez com seu modelo funcionando no Watson Machine Learning Service e com a submissão realizada, o avaliador do respectivo desafio irá fazer inferências no modelo submetido a partir de um dataset de avaliação. A nota do desafio é uma função que compara o valor inferido pelo modelo submetido com o valor real do dataset, avaliando o quão distante o modelo está da realidade e o <img src="https://render.githubusercontent.com/render/math?math=R^2"> do mesmo, que avalia o quão bem o modelo descreve o conjunto de dados.

## 3. Requisitos

Para iniciar o desafio você deverá primeiramente cumprir os seguintes itens:

- Registrar-se na [IBM Cloud](https://ibm.biz/desafio-data-science-bb).

## 4. Informações sobre limites do Watson Studio

Para realizar o desafio, você irá utilizar um plano gratuito do Watson Studio. Para o uso deste plano, há um limite de 50 horas da unidade de processamento definida pela precificação do Watson Studio. As 50 horas devem mais que suficientes para completar os quatro desafios utilizando os tempos de execuções de menos custo e não abusando, no entanto, disponibilizamos a tabela abaixo para você ficar ciente do que pode estar utilizando e não esgotar o limite de seu plano.

|     Ambiente      |        Tempo de execução e capacidade         | Unidades/hora | Possíveis horas de uso até limite do plano |
| :---------------: | :-------------------------------------------: | :-----------: | :----------------------------------------: |
|     Notebook      |  Default Python 3.6 Free - 1 vCPU e 4 GB RAM  |      0.5      |                    100                     |
|     Notebook      |   Default Python 3.6 S - 4 vCPU e 16 GB RAM   |       2       |                     25                     |
|     Notebook      |     Default R 3.4 XS - 2 vCPU e 8 GB RAM      |       1       |                     50                     |
|     Notebook      |     Default R 3.6 S - 4 vCPU e 16 GB RAM      |       2       |                     25                     |
|     R Studio      |    Default RStudio XS - 2 vCPU e 8 GB RAM     |       1       |                     50                     |
| SPSS Modeler Flow |              4 vCPU e 16 GB RAM               |       2       |                     25                     |
|   Data Refinery   | Default Data Refinery XS - 3 vCPU e 12 GB RAM |      1.5      |                     33                     |

Mais informações podem ser encontradas na [documentação](https://dataplatform.cloud.ibm.com/docs/content/wsj/analyze-data/track-runtime-usage.html).

## 5. Desafio

(Sugestão: subtituir por vídeo discutindo sobre DS/ML, produção IBM)

Neste repositório há quatro desafios, dois básicos e dois intermediários, juntos eles cobrem o que é mais utilizado e algumas situações onde é possível aplicar ciência de dados no dia dia.

Para completar o desafio, você irá importar o projeto com o notebook e o dataset disponibilizado neste repositório, portanto este readme contêm a sequência de passos necessários para que esta tarefa se complete. Faça o download do dataset pois ele será utilizado mais tarde, por enquanto vamos nos concentrar em importar o notebook para o Watson Studio, que será o ambiente onde o desafio deverá ser executado.

1. Primeiro faça o download do arquivo Zip contido dentro da pasta de cada desafio contida neste repositório.
    1. [Desafio 1](https://gitlab.com/JoaoPedroPP/teste-ds/-/raw/master/Desafio-1/desafio-1.zip?inline=false)
    2. [Desafio 2](https://gitlab.com/JoaoPedroPP/teste-ds/-/raw/master/Desafio-2/desafio-2.zip?inline=false)
    3. [Desafio 3](#)
    4. [Desafio 4](#)

2. Acesse sua conta na [IBM Cloud](https://ibm.biz/desafio-data-science-bb). Caso ainda não tenha crie uma conta [aqui](https://ibm.biz/desafio-data-science-bb).

3. Ao logar você deverá ver o seu dashboard. Vamos clicar em catálogo.
![img-dash](./support/img/img-dash.png)

4. Procure pelo Watson Studio pois ele será a ferramente de construção dos modelos. Assim que encontra-lo clique nele.
![img-wscatalog2](./support/img/img-wscatalog2.png)

5. De um nome para sua instancia e quando estiver pronto clique em 'Create'.
![img-wscreation](./support/img/img-wscreation.png)

6. Assim que seu 'Watson Studio' carregar a página clique em 'Get Started'.
![img-ws](./support/img/img-ws.png)

7. Esta é a tela inicial do Watson Studio, para que possamos utilizar suas ferramentas precisamos criar um projeto, para isso clique em 'Create a project'.
![img-proj](./support/img/img-proj.png)

8. Vamos criar um projeto a partir de um existente clicando em 'Create a project from a sample file'.
![img-empt](./support/img/img-empt.png)

9. Faça o upload do arquivo zip baixado do desafio que desja resolver. Caso você não tenha um [Object Storage](https://cloud.ibm.com/catalog/services/cloud-object-storage) será preciso criar um, para isso clique em 'Add'.
![img-uploadzip](./support/img/uploadzip.png)

10. Você será direcionado para a tela de criação do 'Object Storage', lá clique em 'Create'.
![img-obcreation](./support/img/img-obcreation.png)

11. De um nome para o seu 'Object Storage' e clique em 'Confirm'.
![img-obconfirm](./support/img/img-obconfirm.png)

12. Clique em 'Refresh' e seu Object Sotrage deve aparecer. Em seguida clique em 'Create'.
![img-create-prj-ws](./support/img/create-prj-ws.png)

13. Você deverá ver uma mensagem preparando o ambiente de excução e assim que estiver carregado basta clicar em 'View new project'.
![img-loading-prj](./support/img/loading-prj.png)

14. Quando página carregar você verá o overview do porjeto, clique em 'Assets' para visulaizar o conteúdo do projeto.
![img-assets](./support/img/assets.png)

15. Assim que a página carregar, você deverá ver os notebook e os datasets disponibilizado para o desafio. Está sendo fornecido dois notebooks, um em python e outro em R, você deve escolher qual linguagem utilizar e abrir um dos notebooks. Dentro do notebook escolhido você vai encontrar o tema do desafio e o processo de submissão do desafio para avaliação. O restante das instruções são relativas a temática do desafio e elas se encontram dentro de cada notebook fornecido, ou seja, a partir deste ponto o resto das instruções se encontra no notebook de resolução dos desafios.
![img-final](./support/img/final.png)

16. Caso você não editar o notebook basta clicar no ícone no canto superior direito em formato de lápis que ele estará pronto para uso. Boa sorte!
![img-last](./support/img/ws-last.png)

## License

Copyright 2019 IBM

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
